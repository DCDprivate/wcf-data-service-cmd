﻿using entityframeworkdatabase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace miscellaneoustasks
{
    public class requesters
    {
        public static requesters singleton;

        public HttpClient httprequester { get; private set;}
        public postgresdatabase databaserequester { get; private set; }

        static requesters()
        {
            singleton = new requesters();
        }

        private requesters()
        {
            httprequester = new HttpClient();
            databaserequester = new postgresdatabase(ConfigurationManager.ConnectionStrings["postgres-key"]
                .ConnectionString);
        }

        ~requesters()
        {
            httprequester.Dispose();
            databaserequester.Dispose();
        }
    }
}
