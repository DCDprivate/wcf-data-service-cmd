﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.Services;
using entityframeworkdatabase.models;
using entityframeworkdatabase;
using System.ServiceModel.Channels;
using System.Data.Services.Common;
using miscellaneoustasks;

namespace yo
{
    public class httplayer : DataService<postgresdatabase>
    {                
        protected override void HandleException(HandleExceptionArgs args)
        {
            requesters.singleton
                .databaserequester
                .authusers
                .Add(new authuser
                {
                    first_name = "hi"
                });
        }

        public static void InitializeService(DataServiceConfiguration config)
        {
            config.UseVerboseErrors = true;
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        }
    }
}
