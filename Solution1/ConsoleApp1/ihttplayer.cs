﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace miscellaneoustasks.crudlayer
{
    [ServiceContract]
    public interface ihttplayer
    {
        [OperationContract]
        string SayHello(string name);
    }
}
