﻿using miscellaneoustasks.crudlayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class start
    {
        public static servicehost crudlayer;

        public start()
        {
            var baseaddresses = new Uri[] {
            new Uri("http://localhost:51039/sup")
        };
            crudlayer = new servicehost(baseaddresses);
            crudlayer.Open();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("hi");
            Console.ReadLine();
        }

        ~start()
        {
            crudlayer.Close();
        }
    }
}
