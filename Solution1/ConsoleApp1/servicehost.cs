﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Services;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using yo;

namespace miscellaneoustasks.crudlayer
{
    public class servicehost : DataServiceHost
    {
        public servicehost(Uri[] baseaddresses) : base(typeof(httplayer), baseaddresses)
        { //crashes when I invoke base.AddDefaultEndpoints
            ServiceMetadataBehavior behaviour = new ServiceMetadataBehavior();
            behaviour.HttpGetEnabled = true;
            //behaviour.HttpsGetEnabled = true;
            behaviour.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            Description.Behaviors.Add(behaviour);
            var context = this;
        }        
    }
}
