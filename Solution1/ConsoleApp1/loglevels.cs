﻿namespace miscellaneoustasks
{
    public enum loglevels
    {
        info,
        warning,
        error,
        critical
    }
}