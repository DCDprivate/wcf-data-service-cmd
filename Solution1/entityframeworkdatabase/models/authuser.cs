namespace entityframeworkdatabase.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class authuser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public authuser()
        {

        }

        public int id { get; set; }

        public string password { get; set; }

        public DateTime? last_login { get; set; }

        public bool is_superuser { get; set; }

        public string username { get; set; }

        public string first_name { get; set; }

        public string last_name { get; set; }

        public string email { get; set; }

        public bool is_staff { get; set; }

        public bool is_active { get; set; }

        public DateTime date_joined { get; set; }
    }
}
