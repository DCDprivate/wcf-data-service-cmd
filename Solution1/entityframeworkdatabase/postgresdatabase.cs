namespace entityframeworkdatabase
{
    using entityframeworkdatabase.models;
    using Npgsql;
    using System;
    using System.Data.Entity;
    using System.Linq;


    public class postgresdatabase : DbContext
    {

        public postgresdatabase(string connectionstring)
            : base(connectionstring)
        {
        }

        public virtual DbSet<authuser> authusers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<authuser>()
                .ToTable("back_auth_user");

            base.OnModelCreating(modelBuilder);
        }
    }

    public class NpgSqlConfiguration : DbConfiguration
    {
        public NpgSqlConfiguration()
        {
            SetProviderFactory("Npgsql", NpgsqlFactory.Instance);
            SetProviderServices("Npgsql", provider: NpgsqlServices.Instance);
            SetDefaultConnectionFactory(new NpgsqlConnectionFactory());
        }
    }
}